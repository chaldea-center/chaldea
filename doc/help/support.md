# 支持Chaldea

如果本项目对你有帮助并希望支持本项目，你可以：
- [Starring](https://github.com/chaldea-center/chaldea/stargazers) the Github repo
- 在 [Google Play](https://play.google.com/store/apps/details?id=cc.narumi.chaldea) 或 [App Store](https://apps.apple.com/us/app/chaldea/id1548713491?itsct=apps_box&itscg=30200) 上评分/评价.
- 分享给你的好友
- 捐赠

## 捐赠
如果希望通过捐赠以支持本项目，请添加留言以注明用于捐赠Chaldea应用。

### Buy me a coffee
[![ko-fi](resource://res/img/md/kofi2.webp)](https://ko-fi.com/G2G152BDO)

### WeChat Pay/微信
![Wechat Pay](resource://res/img/md/wechat_pay.webp)

### Alipay/支付宝
![Alipay](resource://res/img/md/alipay.webp)

### B站充电
[![Bilibili Charge](resource://res/img/md/bilicharge.webp)](https://space.bilibili.com/3785253)


## 联系方式
如果有任何问题或希望退回，可以通过以下方式联系

- Email: [narumi@chaldea.cc](mailto:narumi@chaldea.cc)
