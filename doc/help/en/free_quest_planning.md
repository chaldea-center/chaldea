## Free Quest Planning

*The result is for reference only*
- **Planning/efficiency**:
    - Planning: Set the count of items for planning
    - Efficiency: Set the weight of each item
- **Min AP**: Filter quests with lower AP, but ensure that at least one quest for each item
- **Quest Limit**: items that have not been installed in this progress will be removed from the plan
- **Planning goals** (planning only): minimum AP, minimum count of battles
- **Efficiency type** (efficiency only): per 20AP drop rate or per quest drop rate
- **Blacklist** (planning page only): quest blacklist
- Click the item name to switch the item, click icon to view details
