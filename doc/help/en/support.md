# Support Chaldea

If this project is helpful to you and want to support it, you can:
- [Starring](https://github.com/chaldea-center/chaldea/stargazers) the Github repo
- Rate/review on [Google Play](https://play.google.com/store/apps/details?id=cc.narumi.chaldea) or [App Store](https://apps.apple.com/us/app/chaldea/id1548713491?itsct=apps_box&itscg=30200)
- Share to your friends
- Donation

## Donation
Donation is also welcomed, please leave a message to let me know that's donated for Chaldea.

### Buy me a coffee
[![ko-fi](resource://res/img/md/kofi2.webp)](https://ko-fi.com/G2G152BDO)

### WeChat Pay/微信
![Wechat Pay](resource://res/img/md/wechat_pay.webp)

### Alipay/支付宝
![Alipay](resource://res/img/md/alipay.webp)

### Bilibili Charge
[![Bilibili Charge](resource://res/img/md/bilicharge.webp)](https://space.bilibili.com/3785253)


## Contact me
If you have any question or want the donation back, please contact me:

- Email: [narumi@chaldea.cc](mailto:narumi@chaldea.cc)
